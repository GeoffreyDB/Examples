#!/usr/bin/perl

use strict;
use warnings;
use DBI;
use FindBin;
use lib "$FindBin::Bin/../../lib";
use Geoffrey;

my $dbh = DBI->connect( "dbi:SQLite:database=league.sqlite" );
Geoffrey->new( dbh => $dbh)->read( $FindBin::Bin . '/../web_test_framework' );