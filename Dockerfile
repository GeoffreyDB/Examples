FROM ubuntu:17.04

MAINTAINER Mario Zieschang <mziescha@cpan.org>

RUN     apt-get update && apt-get install -y perl g++ curl make libssl-dev \
    &&  curl -L https://cpanmin.us | perl - App::cpanminus \
    &&  rm -rf  /tmp/* /var/tmp/* /root/.cpanm/* /usr/share/man/* /usr/local/share/man/*

RUN  cpanm -v https://gitlab.com/GeoffreyDB/Geoffrey/archive/master.tar.gz \
    &&  rm -rf  /tmp/* /var/tmp/* /root/.cpanm/* /usr/share/man/* /usr/local/share/man/*

